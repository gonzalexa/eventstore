# Event Store
An open-source and functional database in Java.

# Description 
Event Store is an open-source and functional database in Java. It provides a platform to create and view programs in Java. The following steps will help other contributors recreate the Event Store project on their own machines. 

# Table of Contents
[[_TOC_]]

# Prerequisites
Clone the repository
'git clone https://gitlab.com/mwillema/eventstore.git'

# How to Build Event Store
run 'mvn $MAVEN_CLI_OPTS compile'

# How to Run Event Store
run 'mvn $MAVEN_CLI_OPTS deploy'

# How to Test Event Store
run 'mvn $MAVEN_CLI_OPTS test' 

# Contributors
Marco Willemart 

Julien Meulemans

# Contact Information
marco.willemart@icloud.com 
marco.willemart@unamur.be

jmeulema@gmail.com

# Event Store License
Apache License Version 2.0, January 2004
http://www.apache.org/licenses/
